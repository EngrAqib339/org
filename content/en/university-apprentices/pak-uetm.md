---
title: PAK - UET Mardan 🎓
description: 
position: 07
category: University Apprentices
spr2021:
  mentors:
    - name: Arsala
      avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
      github: https://github.com/ArsalaBangash
      gitlab: https://gitlab.com/ArsalaBangash
      linkedin: https://linkedin.com/in/ArsalaBangash

    - name: Adil Shehzad
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7507821/avatar.png
      github: adilshehzad786
      gitlab: adilshehzad
      linkedin: adilshehzad7

  apprentices:
    - name: Faraz Ahmad Khan
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7891204/avatar.png
      github: https://github.com/farazahmadkhan15
      gitlab: https://gitlab.com/farazahmadkhan15
      linkedin: https://www.linkedin.com/in/farazahmadkhan15/

    - name: Saad Waseem
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8337704/avatar.png
      github: https://github.com/saad-waseem-12
      gitlab: https://gitlab.com/saad-waseem-12
      linkedin: https://www.linkedin.com/in/firebelias12
      
    - name: Wasim Khan
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8335398/avatar.png
      github: https://github.com/Artaghal
      gitlab: https://gitlab.com/Artaghal
      linkedin: https://www.linkedin.com/in/wasim-cybernoob/


    - name: Omar-Bangash
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8344999/avatar.png?width=400
      github: https://github.com/Omar-Bangash
      gitlab: https://gitlab.com/Omar-Bangash
      linkedin: https://www.linkedin.com/in/omar-bangash-7b3139192/

---

## Spring 2021 Cohort

### Mentors

<team-profiles :profiles="spr2021.mentors"></team-profiles>

### Apprentices

<team-profiles :profiles="spr2021.apprentices"></team-profiles>
